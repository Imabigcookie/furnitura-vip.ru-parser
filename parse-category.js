const parseProductInfo = require('./parse-product-info')

parseCategory = async (page, link) => {
  await page.goto(link)

  const productLinks = await page.$$eval('#content1 .wrap > div > div > .categ_2_1 > a', links => links.map(link => link.href))

  for (const productLink of productLinks) {
    await parseProductInfo(page, productLink)
  }

  console.log(`Page ${link} parsed`)
  await page.goto(link)
  try {
    const categoryNextPageLink = await page.$eval('p > a.pages1:last-child', link => link.innerText === 'Вперед >' ? link.href : null)

    categoryNextPageLink && await parseCategory(page, categoryNextPageLink)
  } catch (err) {
  }
}

module.exports = parseCategory
