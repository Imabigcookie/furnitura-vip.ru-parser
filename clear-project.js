const fs = require('fs')
const XlsxPopulate = require('xlsx-populate');

module.exports = async () => {
  if (fs.existsSync('./products.xlsx')) {
    fs.unlinkSync('./products.xlsx');
  }

  fs.rmdirSync('./documents', { recursive: true })

  fs.mkdirSync('./documents')
  fs.mkdirSync('./documents/Exceptions')

  const workbook = await XlsxPopulate.fromBlankAsync()
  const sheet = workbook.sheet(0)

  sheet.row(1).height(25)
  sheet.column('A').width(15)
  sheet.column('B').width(50)
  sheet.column('C').width(30)
  sheet.column('D').width(30)
  sheet.column('E').width(30)
  sheet.column('F').width(30)
  sheet.column('G').width(30)
  sheet.column('H').width(50)
  sheet.column('I').width(50)
  sheet.cell('A1').value('Артикул').style("bold", true)
  sheet.cell('B1').value('Название товара').style("bold", true)
  sheet.cell('C1').value('Категория').style("bold", true)
  sheet.cell('D1').value('Подкатегория 1').style("bold", true)
  sheet.cell('E1').value('Подкатегория 2').style("bold", true)
  sheet.cell('F1').value('Подкатегория 3').style("bold", true)
  sheet.cell('G1').value('Подкатегория 4').style("bold", true)

  await workbook.toFileAsync('./products.xlsx')
}
