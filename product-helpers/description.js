module.exports = async (page) => {
  try {
    const resultObjDescription = {}
    let description = await page.$eval('#content1 > div.view_firm > div:nth-child(2) > div:last-child > p', p => p.innerText)

    description = description.split('\n')

    description.forEach(line => {
      if (!line.length) return

      const lineArray = line.split(': ')

      resultObjDescription[lineArray[0]] = lineArray[1]
    })

    return resultObjDescription
  } catch (err) {
    return null
  }
}
