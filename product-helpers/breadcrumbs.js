module.exports = async (page) => {
  const allBreadcrumbs = await page.$$eval('#titles > a', links => links.map(link => link.innerText))

  allBreadcrumbs.splice(0, 2)

  return allBreadcrumbs
}
