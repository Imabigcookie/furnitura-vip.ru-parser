const fs = require('fs');
const axios = require('axios');
const path = require('path')

downloadImage = async (link, index, productCode) => {
  const url = link
  const fileExt = link.split('.')[2]
  let filePath

  if (productCode.indexOf('*') > -1) {
    filePath = path.resolve(`documents/Exceptions/${productCode.replace(/\*/g, '+')}/images`, `image_${index}.${fileExt}`)
  } else {
    filePath = path.resolve(`documents/${productCode}/images`, `image_${index}.${fileExt}`)
  }
  const writer = fs.createWriteStream(filePath)

  const response = await axios({
    url,
    method: 'GET',
    responseType: 'stream',
  })

  response.data.pipe(writer)

  return new Promise((res, rej) => {
    writer.on('finish', res)
    writer.on('error', rej)
  })
}

module.exports = async (page, productCode) => {
  const downloadLinksArray = await page.$$eval('a[rel="prettyPhoto[photo1]"]', links => links.map(link => link.href))
  let dirPath

  if (productCode.indexOf('*') > -1) {
    dirPath = path.resolve(`documents/Exceptions/${productCode.replace(/\*/g, '+')}/images`)
  } else {
    dirPath = path.resolve(`documents/${productCode}/images`)
  }

  fs.mkdirSync(dirPath, { recursive: true })

  await Promise.all(downloadLinksArray.map((link, index) => downloadImage(link, index + 1, productCode)))
}
