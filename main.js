const puppeteer = require('puppeteer');
const catalogParser = require('./parse-category');
const clearProject = require('./clear-project');

(async () => {
  await clearProject()

  const browser = await puppeteer.launch()
  const page = await browser.newPage()

  await page.goto('https://furnitura-vip.ru/catalog')

  // const catalogLinks = await page.$$eval(
  //   '#content1 > .cat_cat > a',
  //   links => links.map(link => link.href)
  // )
  const catalogLinks = ['https://furnitura-vip.ru/catalog/ruchki-mebelnye']

  for (const catalogLink of catalogLinks) {
    await catalogParser(page, catalogLink)
    console.log('Сategory parsed')
  }


  await browser.close()
  console.log('Done!')
})();
