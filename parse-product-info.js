const XlsxPopulate = require('xlsx-populate');
const getCategories = require('./product-helpers/breadcrumbs');
const getDescription = require('./product-helpers/description');
const getCharacteristic = require('./product-helpers/characteristic');
const getRelatedProducts = require('./product-helpers/related-products');
const downloadAllProductPhotos = require('./product-helpers/product-photos');
const downloadAllProductFiles = require('./product-helpers/product-files');

module.exports = async (page, link) => {
  await page.goto(link)

  const productName = await page.$eval('#content1 > .view_firm > h1', h1 => h1.innerText)
  const productCategories = await getCategories(page)
  const productDescription = await getDescription(page)

  const productCode = productDescription ? productDescription['Артикул'] : ''

  if (!productCode || !productDescription) {
    console.log(`Error on ${link}`)

    return
  }

  await downloadAllProductPhotos(page, productCode)

  const xlsx = await XlsxPopulate.fromFileAsync('./products.xlsx')
  const sheet = xlsx.sheet(0)
  const rowNumber = sheet._rows.length

  sheet.cell(`A${rowNumber}`).value(productCode)
  sheet.cell(`B${rowNumber}`).value(productName)

  productCategories.forEach((category, index) => {
    sheet.cell(rowNumber, index + 3).value(category)
  })

  Object.keys(productDescription).forEach(key => {
    const newKey = key.replace('(', '').replace(')', '')
    const searchStr = new RegExp(newKey)
    const foundCells = sheet.row(1).find(searchStr)
    const lastCellNumber = sheet.row(1).maxUsedColumnNumber() + 1

    if (!foundCells.length) {
      sheet.cell(1, lastCellNumber).value(newKey).style("bold", true)
      sheet.cell(rowNumber, lastCellNumber).value(productDescription[key])
    } else {
      const columnNumber = foundCells[0].columnNumber()

      sheet.cell(rowNumber, columnNumber).value(productDescription[key])
    }
  })

  await xlsx.toFileAsync('./products.xlsx')
  console.log(`Product ${link} done!`)
}
